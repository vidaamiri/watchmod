<?php include('header.php');?>
<?php include('config.php');?>

<?php
$postid=$_GET["postid"];
$posrsql="SELECT * FROM `postcontent` WHERE `id`=$postid;" ;
$postquery=mysqli_query($link,$posrsql);
while($postfetch=mysqli_fetch_assoc($postquery))
{
?>   
    <div class="container m-auto my-5 page-body">
        <div class="row text-center">
            <div class="col-12 my-4" >
                <img src="uploads/images/post/<?php echo $postfetch['src'] ?>" alt="<?php echo $postfetch["title"] ?>" class="d-none d-md-block m-auto" style="height: 500px;">
                <img src="uploads/images/post/<?php echo $postfetch['src'] ?>" alt="<?php echo $postfetch["title"] ?>" class="d-none  d-sm-block d-md-none m-auto" style="height: 300px;">
                <img src="uploads/images/post/<?php echo $postfetch['src'] ?>" alt="<?php echo $postfetch["title"] ?>" class="d-black d-sm-none m-auto" style="height: 200px;">
            </div>
            <div class="col-12 col-md-auto text-center">
                <div class="card-body">
                    <h5 class="card-title title-page" style="color: #63c3a7;"><?php echo $postfetch["title"] ?></h5>
                    <p class="card-text"><?php echo $postfetch["content"] ?></p>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<?php include('footer.php');?>