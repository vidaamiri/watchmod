<?php
$posrsql="SELECT * FROM `postcontent` ORDER BY `id` DESC";
$postquery=mysqli_query($link,$posrsql);
while($postfetch=mysqli_fetch_assoc($postquery))
{
?>   
 <!-- postcontent -->
 <div class="container container-post my-5">
        <div class="card mb-3 postcontent">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
            <div class="row g-0 p-3">
                <div class="col-lg-3 col-md-4 col-sm-5 co-12 ">
                    <img src="uploads/images/post/<?php echo $postfetch['src'] ?>"  class="rounded-start image-postcontent" alt=<?php echo $postfetch["title"] ?>>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-7 col-12">
                    <div class="card-body" style="text-align: right;"> 
                        <h5 class="card-title title-postcontent"><?php echo $postfetch["title"] ?></h5>
                        <p class="card-text px-4"><?php echo post_content($postfetch["content"]); ?></p>
                        <div class="btn-postcontent ">
                            <a class="btn" href=<?php echo "page.php?postid=$postfetch[id]" ?>>ادامه مطلب ...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>    