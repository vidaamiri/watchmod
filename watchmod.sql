-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2021 at 04:35 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watchmod`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'vida', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `link`) VALUES
(9, 'خانه', 'https://getbootstrap.com/docs/5.0/components/buttons/');

-- --------------------------------------------------------

--
-- Table structure for table `postcontent`
--

CREATE TABLE `postcontent` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `src` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `postcontent`
--

INSERT INTO `postcontent` (`id`, `title`, `src`, `content`) VALUES
(17, 'جدیدترین ساعت مچی هابلوت با ظاهری شیشه و از جنس یاقوت', 'e58d8e351fbffb38b78940cfc850bba11ec40392.jpg', 'تخصص هابلوت در ساخت بدنه‌های ساعت مچی از یاقوت کبود، فراتر از حد تصور است. تولیدکننده مستقر در نیون جزو اولین تولیدکننده‏ایی بود که چنین بدنه‌هایی را در مقیاسی صنعتی در سال 2016 تولید کرد. در این زمینه به مدت 5 سال است که پیشرو باقی مانده است. اما اخیراً هابلوت برای ما چه کاری انجام داده است؟ پاسخ این سؤال چند روز پیش و در نمایشگاه مجازی واچز اند واندرز داده شد. یک ساعت با بدنه و بند ساخته شده از یاقوت کبود. در اینجا نگاهی نزدیکتر به ساعت مچی Big Bang Integral Tourbillon Full Sapphire می‌اندازیم. ساعتی انقلابی که ماده استفاده شده در آن فقط نوک یک کوه نوآوری است.\r\n\r\nمشخصات فنی ساعت:\r\nدر این ساعت مچی شیک از دو مورد مهم‌ترین پیشرفت‎های اخیر هابلوت استفاده شده است. بدنه یکپارچه با بند که ژانویه پیشین در مدل‌های ساعت مچی کرنوگرافی Big Bang Integral ارائه شده و آخرین مدل کالیبر اتوماتیک توربیلون (کالیبر Caliber HUB6035) که سال قبل در مدل‌های سه گانه Big Bang ارائه شده است. از جمله دیگر موارد مهم، استفاده از بدنه یاقوت کبود می‌باشد. بدنه صفحه نیز از یک دیسک یاقوت کبود شفاف تشکیل شده است. عقربه‌های این ساعت مچی مردانه نیز از جنس رودیم با پوشش Super-LumiNova می‌باشد.'),
(18, 'ساعت مچی برای مسیریابی و کوهنوردی از مونت‌بلانک (Montblanc)', '3dd5fb400b639c9acffb0d3320bd3e6bdad33e39.jpg', 'همانند ساعت‌ مچی های Geosphere قدیمی، آخرین نسخه نیز دارای ساختار بدنه‌ای سخت و صفحه ای با دو نیم‌ کره است. این موضوع به دارندگان این ساعت مچی دو زمانه کمک می‌کند که ساعت‌های زمانی مناطق مختلف در هر یک از دو نیم‌ کره زمین را مشاهده کنند. رنگ منحصر به فرد ساعت از کوهنورد ایتالیایی به نام رینولد مسنر و سفر انفرادی وی به صحرای گوبی در سال 2004، الهام گرفته شده است. این دومین Geosphere الهام گرفته از سفرهای مسنر است. اولین ساعت دارای یک صفحه آبی بود و مربوط به تلاش کوهنوردان برای فتح هفت بلندترین قله جهان بود.'),
(21, 'راهنمای خرید ساعت مچی مناسب با هر استایل', 'dddc86d1388a2c0844af68a10c09fea794c1a6f2.jpg', 'حتما از تاثیر ساعت مچی در بهتر دیده شدن استایل هر شخص اطلاع دارید. اما خرید ساعت مچی مردانه یا زنانه مختص استایل خودتان، نیازمند به آشنایی با انواع سبک ها در دنیای ساعت دارد. در این مقاله  به شما کمک می کنیم، تا در هنگام انتخاب ساعت مچی بدانید که هدفتان چیست و قرار است آن ساعت مچی در کدام استایل شما مورد استفاده قرار بگیرد.\r\n\r\nپیشنهاد ما برای دوستداران ساعت مچی، داشتن حداقل یک ساعت از هر 3 دسته زیر است:\r\nساعت مچی کلاسیک\r\nساعت مچی ورزشی (اسپرت)\r\nساعت مچی فشن');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `src` varchar(500) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `src`) VALUES
(7, 'محبوب ترین کالکشن', '7ba4559b2f84cb8b5110917b57f06ed1c54e60dc.jpg'),
(8, 'محبوب ترین کالکشن', '754d21effab0591dc8f7ad8c949a6544c7fe556b.jpg'),
(9, 'محبوب ترین کالکشن', '219e8f79cda65748c8d15e9d94e7c83efb91ff60.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `specialpost`
--

CREATE TABLE `specialpost` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `src` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `specialpost`
--

INSERT INTO `specialpost` (`id`, `title`, `src`, `content`) VALUES
(17, 'ساعت ست', '0679761852995c03bb1366c628718ad232f80243.jpg', 'ساعتمونو ست کنیم'),
(19, 'ساعت مردانه', '8d9bdd0f346a2a68b258ec4c82c60574ff11d359.jpg', 'ساعتی برای آقایان'),
(20, 'ساعت زنانه', '2e7d27af6671b186f969f5234730ae2217e5ce56.jpg', 'ساعتی برای بانوان');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postcontent`
--
ALTER TABLE `postcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialpost`
--
ALTER TABLE `specialpost`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `postcontent`
--
ALTER TABLE `postcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `specialpost`
--
ALTER TABLE `specialpost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
