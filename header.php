<?php include('config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="asset/css/stayle.css">
    <title>WatchMod</title>
</head>

<body onload="documentLoad()">
    <!-- navbar -->
    <nav class="navbar navbar-expand-sm ">
        <div class="container">
            <a class="navbar-brand" href="#">WatchMod</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span>
                    <li class="fa fa-bars"></li>
                </span>
            </button>      
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- <ul class="navbar-nav">
                    <li class="nav-item mx-sm-3 mx-0">
                        <a class="nav-link active" aria-current="page" href="#">خانه</a>
                    </li>
                    <li class="nav-item dropdown mx-sm-3 mx-0">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            محصولات
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">ساعت </a></li>
                            <li><a class="dropdown-item" href="#">زیور آلات</a></li>
                        </ul>
                    </li> -->
                <!-- </ul> -->
                 <ul class="navbar-nav">
                    <?php
                        include('config.php');
                        $menu="SELECT * FROM `menu` ORDER BY `id` DESC";
                        $menuquery=mysqli_query($link,$menu);
                        while($menufetch=mysqli_fetch_assoc($menuquery))
                        {
                    ?>
                    <li class="nav-item mx-sm-3 mx-0">
                        <a class="nav-link active" aria-current="page" href=<?php echo $menufetch["link"] ?>><?php echo $menufetch["title"] ?></a>
                    </li>
                    <?php
                        }
                    ?>    
                 </ul>  
                 <li class="nav-item dropdown mx-4" style="list-style: none;">
                        <a class="nav-link" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal"
                            data-bs-whatever="@mdo">ورود</a>
                        <div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                       <h5 class="modal-title" id="exampleModalLabel">صفحه ی ورود</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close" style="position: absolute;left: 20px;"></button>
                                    </div>
                                    
                                    <div class="modal-body">
                                        <form id="login_form">
                                            <span id="error_login_form" class="d-flex justify-content-center text-danger">
                                            </span>
                                            <div class="mb-3">
                                                <label for="login_username" class="col-form-label">نام کاربری :</label>
                                                <input  name="username" type="text" class="form-control" id="login_username" onchange="changeFieldForm()">
                                            </div>
                                            <div class="mb-3">
                                                <label for="login_password" class="col-form-label">رمز عبور :</label>
                                                <input name="password" type="text" class="form-control" id="login_password" onchange="changeFieldForm()">
                                            </div>
                                            <input type="submit" value="ورود" name="btnlogin" class="btn btn-warning">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li> 
            </div>
        </div>
    </nav>