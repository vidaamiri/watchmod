<footer class="footer p-3 ">
            <div class="container pt-0 d-flex">
                <div class="row w-100 p-3  d-md-flex  d-inline justify-content-center ">
                    <div class="col-md-6 col-sm-12 ">
                        <label> اخرین پست ها</label>
                        <ul class="p-0">
                        <?php
                $postsql="SELECT * FROM `postcontent` ORDER BY `id` DESC";
                $postquery=mysqli_query($link,$postsql);
                while($postfetch=mysqli_fetch_assoc($postquery))
                 {
                ?>  
                        <li><a href=<?php echo "./page.php?postid=$postfetch[id]" ?>><?php echo $postfetch["title"] ?></a></li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <label>درباره ما</label>
                        <p>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                        </p>
                    </div>
                </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
        const documentLoad = () => {
                const form_login = document.getElementById("login_form");
                form_login.addEventListener("submit", (event) => {
                        event.preventDefault();
                        let username = document.getElementById("login_username").value;
                        let password = document.getElementById("login_password").value;
                        let error_tag = document.getElementById("error_login_form");

                        if (!username) {
                                error_tag.innerText = "نام کاربری خالی است";
                        } else if (!password) {
                                error_tag.innerText = "رمز عبور خالی است";
                        }else{
                                let url = "check.php";
                                let params = `loginform=true&username=${username}&password=${password}`;
                                let xhttp = new XMLHttpRequest();
                                xhttp.open("POST", url, true);

                                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                xhttp.onreadystatechange = () => { //Call a function when the state changes.
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                                let response = JSON.parse(xhttp.responseText);
                                                if (response.status) {
                                                        document.location.href = `${document.location.origin}/${(document.location.pathname).split("/")[1]}/admin/panel.php?adminpid=${response.id}`;
                                                } else {
                                                        if (response.error == "empty") {
                                                                error_tag.innerText = "نام کاربری و رمز عبور خالی است";
                                                        } else if (response.error == "incorrect") {
                                                                error_tag.innerText = "نام کاربری یا رمز عبور اشتباه است";
                                                        }else{
                                                                error_tag.innerText = "مشکلی پیش آمده است";
                                                        }
                                                };
                                        };
                                };
                                xhttp.send(params);
                        };
                }, true);
        };
        const changeFieldForm = () => {
                let error_tag = document.getElementById("error_login_form");
                error_tag.innerText = "";
        }
</script>
</body>

</html>