<?php include('config.php'); ?>
<?php
// admin login
if (isset($_POST["loginform"])) {
    if (empty($_POST["username"]) || empty($_POST["password"])) {
        header('Content-Type: application/json');
        echo (json_encode([
            "status" => false,
            "error" => "empty"
        ]));
        exit;
    }
    $adminusername = $_POST["username"];
    $adminpassword = $_POST["password"];
    $admin = "SELECT * FROM `admin` WHERE username='$adminusername' && password='$adminpassword'";
    $adminresult = mysqli_query($link, $admin);
    $adminfeach = mysqli_fetch_assoc($adminresult);
    if ($adminfeach) {
        setcookie("admin", "مدیر وبسایت", time() + (86400 * 7));
        header('Content-Type: application/json');
        echo (json_encode([
            "status" => true,
            "id" => $adminfeach["id"],
        ]));
        exit;
    } else {
        header('Content-Type: application/json');
        echo (json_encode([
            "status" => false,
            "error" => "incorrect"
        ]));
        exit;
    }
}
// admin exit
if (isset($_GET["exit"])) {
    setcookie("admin", "مدیر وبسایت", time() - (86400 * 7));
    header("location:index.php");
    exit;
}
// send post 
if (isset($_POST["btnsendpost"])) {
    if (empty($_POST["titlepost"]) || empty($_POST["contentpost"]) || $_FILES["srcpost"]['size'] < 10) {
        header("location:admin/sendpost.php?empty=1010&size=");
        exit;
    } else {

        $titlepost = $_POST["titlepost"];
        $contentpost = $_POST["contentpost"];
        $dir = "uploads/images/post/";
        function createName($dir, $fileName)
        {
            $ex = explode('.', $fileName);
            $fileName = sha1(microtime()) . '.' . end($ex);
            if (!is_file($dir . '\\' . $fileName)) {
                return $fileName;
            } else {
                return createName($dir, $fileName);
            }
        }
        $filename = createName($dir, $_FILES['srcpost']['name']);

        $type = $ex = explode('.', $filename);
        $FileType = end($type);

        if (!in_array($FileType, ['png', 'gif', 'jpeg', 'jpg'])) {
            header("location:admin/sendpost.php?file-error=4040");
            exit;
        } else {
            $postsql = "INSERT INTO `postcontent` (`title`, `src`, `content`) VALUES ('$titlepost', '$filename', '$contentpost');";
            $postquery = mysqli_query($link, $postsql);

            if ($postquery) {
                move_uploaded_file($_FILES['srcpost']['tmp_name'], $dir . $filename);
                header("location:admin/sendpost.php?ok=1020");
                exit;
            } else {
                header("location:admin/sendpost.php?error=1020");
                exit;
            }
        }
    }
}
// post delet
if (isset($_GET["postid"])) {
    $postdelid = $_GET["postid"];
    $postdel = "DELETE FROM `postcontent` WHERE `postcontent`.`id` = $postdelid";
    $postdelquery = mysqli_query($link, $postdel);
    if ($postdelquery) {
        header("location:admin/postmanage.php?okdel=1010");
        exit;
    } else {
        header("location:admin/postmanage.php?erorrdel=1010");
        exit;
    }
}

// post update
if (isset($_POST["btnupdatedpost"])) {
    if (empty($_POST["title"]) || empty($_POST["src"]) || empty($_POST["content"])) {
        header("location:admin/postmanage.php?empty=1010");
        exit;
    } else {
        $updatepost = $_POST["updatepostid"];
        $updatepost = "UPDATE `watchmod`.`postcontent` SET `title`= '" . $_POST["title"] . "', `src`= '" . $_POST["src"] . "', `content` = '" . $_POST["content"] . "'
        WHERE `postcontent`.`id`=$updatepost;";
        $updatepostresult = mysqli_query($link, $updatepost);
        if ($updatepostresult) {
            header("location:admin/postmanage.php?okupdate=1050");
            exit;
        } else {
            header("location:admin/postmanage.php?errorupdate=1050");
            exit;
        }
    }
}

// navbar
if (isset($_POST["btnnavbar"])) {
    if (empty($_POST["navbartitle"]) || empty($_POST["navbarlink"])) {
        header("location:admin/setting.php?emptynavbar=2020");
        exit;
    }
    $navbartitle = $_POST["navbartitle"];
    $navbarlink = $_POST["navbarlink"];
    $menu = "INSERT INTO `menu` (`id`, `title`, `link`) VALUES (NULL, '$navbartitle', '$navbarlink');";
    $menuresult = mysqli_query($link, $menu);
    if ($menuresult) {
        header("location:admin/setting.php?oknavbar=1050");
        exit;
    } else {
        header("location:admin/setting.php?errornavbar=1050");
        exit;
    }
}
// slider
if (isset($_POST["btnslider"])) {
    if (empty($_POST["slidertitle"]) || $_FILES["sliderimage"]['size'] < 10) {
        header("location:admin/setting.php?emptyslider=2020");
        exit;
    }
    $slidertitle = $_POST["slidertitle"];
    $dir = "uploads/images/slider/";
    function createName($dir, $fileName)
    {
        $ex = explode('.', $fileName);
        $fileName = sha1(microtime()) . '.' . end($ex);
        if (!is_file($dir . '\\' . $fileName)) {
            return $fileName;
        } else {
            return createName($dir, $fileName);
        }
    }
    $filename = createName($dir, $_FILES['sliderimage']['name']);

    $type = $ex = explode('.', $filename);
    $FileType = end($type);

    if (!in_array($FileType, ['png', 'gif', 'jpeg', 'jpg'])) {
        header("location:admin/setting.php?file-error=4040");
        exit;
    } else {
        $slider = "INSERT INTO `slider` (`title`, `src`) VALUES ('$slidertitle', '$filename');";
        $sliderresult = mysqli_query($link, $slider);
        if ($sliderresult) {
            move_uploaded_file($_FILES['sliderimage']['tmp_name'], $dir . $filename);
            header("location:admin/setting.php?okslider=1050");
            exit;
        } else {
            header("location:admin/setting.php?errorslider=1050");
            exit;
        }
    }
}
// send specialpost
if (isset($_POST["btnspecialpost"])) {
    if (empty($_POST["specialposttitle"]) || empty($_POST["specialpostcontent"]) || $_FILES["specialpostimage"]['size'] < 10) {
        header("location:admin/specialpostmanage.php?specialempty=1010&size=");
        exit;
    } else {

        $specialposttitle = $_POST["specialposttitle"];
        $specialpostcontent = $_POST["specialpostcontent"];
        $dir = "uploads/images/specialpost/";
        function createName($dir, $fileName)
        {
            $ex = explode('.', $fileName);
            $fileName = sha1(microtime()) . '.' . end($ex);
            if (!is_file($dir . '\\' . $fileName)) {
                return $fileName;
            } else {
                return createName($dir, $fileName);
            }
        }
        $filename = createName($dir, $_FILES['specialpostimage']['name']);

        $type = $ex = explode('.', $filename);
        $FileType = end($type);

        if (!in_array($FileType, ['png', 'gif', 'jpeg', 'jpg'])) {
            header("location:admin/specialpostmanage.php?file-error=4040");
            exit;
        } else {
            $specialpostsql = "INSERT INTO `specialpost` (`title`, `src`, `content`) VALUES ('$specialposttitle', '$filename', '$specialpostcontent');";
            $specialpostquery = mysqli_query($link, $specialpostsql);

            if ($specialpostquery) {
                move_uploaded_file($_FILES['specialpostimage']['tmp_name'], $dir . $filename);
                header("location:admin/specialpostmanage.php?specialok=1020");
                exit;
            } else {
                header("location:admin/specialpostmanage.php?specialerror=1020");
                exit;
            }
        }
    }
}
// navbar delet
if (isset($_GET["menuid"])) {
    $menudelid = $_GET["menuid"];
    $menudel = "DELETE FROM `menu` WHERE `menu`.`id` = $menudelid";
    $menudelquery = mysqli_query($link, $menudel);
    if ($menudelquery) {
        header("location:admin/show.php?okdelmnu=1010");
        exit;
    } else {
        header("location:admin/show.php?erorrdelmnu=1010");
        exit;
    }
}

// slider delet
if (isset($_GET["sliderid"])) {
    $sliderdelid = $_GET["sliderid"];
    $sliderdel = "DELETE FROM `slider` WHERE `slider`.`id` = $sliderdelid";
    $sliderdelquery = mysqli_query($link, $sliderdel);
    if ($sliderdelquery) {
        header("location:admin/show.php?okdeslider=1010");
        exit;
    } else {
        header("location:admin/show.php?erorrdeslider=1010");
        exit;
    }
}
?> 