<div class="container specialpost mt-4">
    <?php
    $i = 0;
    $specialsql = "SELECT * FROM `specialpost` ORDER BY `id` DESC";
    $specialquery = mysqli_query($link, $specialsql);
    while ($specialfetch = mysqli_fetch_assoc($specialquery)) {
    ?>
        <div class="card m-4 special ">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <img src="uploads/images/specialpost/<?php echo $specialfetch["src"] ?>" alt="<?php echo $specialfetch["title"] ?>" class="card-img special-img">
            <div class="card-body special-content">
                <div class="card-title"><?php echo $specialfetch["title"] ?></div>
                <div class="cord-text"><?php echo $specialfetch["content"] ?></div>
            </div>
        </div>
    <?php
        $i++;
        if ($i >= 3) {
            break;
        }
    }
    ?>
</div>