<?php include('menu.php');?>
<?php include('../config.php');?>
<?php include('../fun.php');?>
<?php
if(!isset($_COOKIE["admin"])){
header("location:../index.php");
exit;
}
?>
 
    <div class="container my-5 p-1 p-md-3 post-manage">
        <!-- <div class="row d-md-flex  d-inline justify-content-center w-100">
            <div class="col-md-6 col-sm-12 p-0">
                <label for="" class="labletag ">عنوان</label>
                <ul>
                    <li><a href="">بهترین طرح و رنگ برای تمامی سلیقه ها</a></li>
                    <li><a href="">بهترین طرح و رنگ برای تمامی سلیقه ها</a></li>
                    <li><a href="">بهترین طرح و رنگ برای تمامی سلیقه ها</a></li>
                    <li><a href="">بهترین طرح و رنگ برای تمامی سلیقه ها</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 p-0">
                <div class="row justify-content-center w-100">
                    <div class="col-auto">
                        <label for="" class="labletag ">ویرایش</label><br>
                        <ul>
                            <li><a href="">ویرایش</a></li>
                            <li><a href="">ویرایش</a></li>
                            <li><a href="">ویرایش</a></li>
                            <li><a href="">ویرایش</a></li>
                        </ul>
                    </div>
                    <div class="col-auto">
                        <label for="" class="labletag ">حذف</label><br>
                        <ul>
                            <li><a href="">حذف</a></li>
                            <li><a href="">حذف</a></li>
                            <li><a href="">حذف</a></li>
                            <li><a href="">حذف</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> -->
        <?php
        // post delet 
        if(isset($_GET["okdel"])){
            echo "<font color=green>پست با موفقیت حذف شد</font>";
        }
        if(isset($_GET["errordel"])){
            echo "<font color=red>مشکل در حذف پست</font>";
        }

        // post update
        if(isset($_GET["empty"])){
            echo "<font color=red>تمام فیلد ها باید پر باشند</font>";
        }
        if(isset($_GET["errorupdate"])){
            echo "<font color=red>مشکل در بروز رسانی پست /font>";
        }
        if(isset($_GET["okupdate"])){
            echo "<font color=green>اطلاعات با موفقیت بروز شد شد</font>";
        }
        ?>
        <table class=" table mt-4 ">
    <thead>
      <tr class="labletag">
        <th scope="col" class="w-50">عنوان</th>
        <th scope="col">حذف</th>
        <th scope="col">ویرایش</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $posrsql="SELECT * FROM `postcontent` ORDER BY `id` DESC";
        $postquery=mysqli_query($link,$posrsql);
        while($postfetch=mysqli_fetch_assoc($postquery)){
    ?>
      <tr>
          <td scope="col"><a href=<?php echo "../page.php?postid=$postfetch[id]" ?>><?php echo $postfetch["title"] ?></a></td>
          <td scope="col"><a href=<?php echo "updatepost.php?postid=$postfetch[id]" ?>>ویرایش</a></td>
          <td scope="col"><a href=<?php echo "../check.php?postid=$postfetch[id]" ?>>حذف</a></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
    </div>

    <body>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    </body>

</html>