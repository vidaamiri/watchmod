<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../asset/css/panel.css">
    <link rel="stylesheet" href="../asset/css/stayle.css">
    <title>WatchMod</title>
</head>
<body style="background: beige;">
<!-- navbar -->
<nav class="navbar navbar-expand-md">
    <div class="container">
        <a class="navbar-brand" href="../index.php">WatchMod</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span>
                    <li class="fa fa-bars"></li>
                </span>
            </button> 
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
            <li class="nav-item mx-sm-3 mx-0">
                    <a class="nav-link " aria-current="page" href="./panel.php">صفحه مدیریت</a>
                </li>
                <li class="nav-item mx-sm-3 mx-0">
                    <a class="nav-link " aria-current="page" href="./specialpostmanage.php">مدیریت پست های ویژه</a>
                </li>
                <li class="nav-item dropdown mx-sm-3 mx-0">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        مدیریت پست
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="./sendpost.php">ارسال پست جدید </a></li>
                        <li><a class="dropdown-item" href="./postmanage.php"> مدیریت پست های قبلی</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown mx-sm-3 mx-0">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        تنظیمات
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="./setting.php">تنظیمات منو و اسلایدر</a></li>
                        <li><a class="dropdown-item" href="./show.php">مدیریت اجزا</a></li>
                    </ul>
                </li>
                <li class="nav-item mx-sm-3 mx-0">
                    <a class="nav-link active" aria-current="page" href=<?php echo "../check.php?exit=3050"?>>خروج</a>
                </li>
            </ul>
        </div>
    </div>
</nav>



