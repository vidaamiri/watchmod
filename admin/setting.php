<?php include('menu.php'); ?>
<?php include('../config.php'); ?>
<?php
if (!isset($_COOKIE["admin"])) {
    header("location:../index.php");
    exit;
}
?>

<div class="container post-manage my-5 " id="navbar-id">
    <label for="" class="labletag m-3 text-center" style="float: right;width: 200px;">navbar</label>
    <br>

    <div class="my-4 mx-auto p-4">
        <?php
        if (isset($_GET["emptynavbar"])) {
            echo "<center><font color=red>تمام فیلد ها باید پر باشند</font></center>";
        }
        if (isset($_GET["errornavbar"])) {
            echo "<center><font color=red>مشکل در ارسال اطلاعات</font></center>";
        }
        if (isset($_GET["oknavbar"])) {
            echo "<center><font color=green>اطلاعات باموفقیت ارسال شد</font></center>";
        }
        ?>
        <form method="post" action="../check.php" class="mb-3">
            <label for="exampleFormControlInput1" class="form-label fw-bold">عنوان</label>
            <input type="text" class="form-control " id="exampleFormControlInput1" name="navbartitle">
            <label for="exampleFormControlInput1" class="form-label fw-bold">لینک</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="navbarlink">
            <input type="submit" value="ارسال" class="btn btn-warning m-3" name="btnnavbar">
        </form>
    </div>
</div>
<div class="container post-manage my-5 ">
    <label for="" class="labletag m-3 text-center" style="float: right;width: 200px;">slider</label>
    <br>

    <div class="my-4 mx-auto p-4">
        <?php
        if (isset($_GET["emptyslider"])) {
            echo "<center><font color=red>تمام فیلد ها باید پر باشند</font></center>";
        }
        if (isset($_GET["errorslider"])) {
            echo "<center><font color=red>مشکل در ارسال اطلاعات</font></center>";
        }
        if (isset($_GET["file-error"])) {
            echo "<center><font color=red>فرمت فایل اشتباه است.</font></center>";
        }
        if (isset($_GET["okslider"])) {
            echo "<center><font color=green>اطلاعات باموفقیت ارسال شد</font></center>";
        }
        ?>
        <form method="post" action="../check.php" enctype="multipart/form-data" class="mb-3">
            <label for="exampleFormControlInput1" class="form-label fw-bold">عنوان</label>
            <input type="text" class="form-control " id="exampleFormControlInput1" name="slidertitle">
            <label for="formFileMultiple" class="form-label">عکس</label>
            <input class="form-control" type="file" id="formFileMultiple" name="sliderimage" multiple>
            <!-- <label for="exampleFormControlInput1" class="form-label fw-bold">محتوا</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="slidercontent"> -->
            <input type="submit" value="ارسال" class="btn btn-warning m-3" name="btnslider">
        </form>
    </div>
</div>

<body>
    <script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>