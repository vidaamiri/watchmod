<?php include('menu.php');?>
<?php include('../config.php');?>

<!-- sendpost -->
<div class="container sendpost p-4">
    <label for="" class="labletag">ویرایش پست</label>
  
    <div class="sendpostbox p-4">
    <?php
       
            if(!isset($_GET["postid"])){
                header("location:postmanage.php");
                exit;
            }
                $postid=$_GET["postid"];
                $posrsql="SELECT * FROM `postcontent` WHERE `id`=$postid;" ;
                $postquery=mysqli_query($link,$posrsql);
                while($postfetch=mysqli_fetch_assoc($postquery))
            {
           
?>
        <form method="post" action="../check.php">
            <label for="">عنوان</label>
            <input type="text" name="title" value="<?php echo $postfetch["title"] ?>" >
            <label for="">ادرس تصویر شاخص</label>
            <input type="text" name="src" value="<?php echo $postfetch["src"] ?>">
            <label for="">متن</label>
            <input type="hidden" name="updatepostid" value="<?php echo $postfetch["id"] ?>">
            <textarea name="content" id="" class="form-control" ><?php echo $postfetch["content"] ?></textarea>
            <input type="submit" value="ارسال اطلاعات" name="btnupdatedpost"
                class="btn btn-outline-warning btn-rounded waves-effect float-right p-2"
                style="border-radius: 20px;font-size: large">
            <a href="./uploadimage.php" type="button" class="btn btn-warning btn-rounded float-right py-2 px-4 "
                style="border-radius: 20px;">آپلود تصویر </a>
        </form>
    </div>
</div>
<?php
            
        }
        ?>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>

</html>