<?php include('menu.php');?>
<?php include('../config.php');?>
<?php include('../fun.php');?>
<?php
if(!isset($_COOKIE["admin"])){
header("location:../index.php");
exit;
}
?>
<!-- navbar -->
    <div class="container my-5 p-1 p-md-3 post-manage">
    <label for="" class="labletag text-center" style="width:300px">تنظیمات منو</label>
    <?php
        if(isset($_GET["okdelmnu"])){
            echo "<center><font color=green> با موفقیت حذف شد</font></center>";
        }
        if(isset($_GET["errordelmnu"])){
            echo "<center><font color=red>مشکل در حذف </font></center>";
        }
    ?>
    <table class=" table mt-4 w-75 m-auto">
    <thead>
      <tr class="labletag">
        <th scope="col">عنوان</th>
        <th scope="col">لینک</th>
        <th scope="col">حذف</th>
      </tr>
    </thead>
    <tbody>
    <?php
            $mnu="SELECT * FROM `menu` ORDER BY `id` DESC";
            $mnuquery=mysqli_query($link,$mnu);
            while($mnufetch=mysqli_fetch_assoc($mnuquery))
            {
                ?>
      <tr>
            <td scope="col"><a href=""><?php echo $mnufetch["title"] ?></a></td>
            <td scope="col"><a href=""><?php echo $mnufetch["link"] ?></a></td>
            <td scope="col"><a href=<?php echo "../check.php?menuid=$mnufetch[id]" ?>>حذف</a></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
    </div>

<!-- slider     -->
<div class="container my-5 p-1 p-md-3 post-manage">
    <label for="" class="labletag text-center" style="width: 300px;">تنظیمات اسلایدر</label>
    <?php
        if(isset($_GET["okdeslider"])){
            echo "<center><font color=green> با موفقیت حذف شد</font></center>";
        }
        if(isset($_GET["erorrdeslider"])){
            echo "<center><font color=red>مشکل در حذف </font></center>";
        }
    ?>
    <table class=" table mt-4 w-75 m-auto">
    <thead>
      <tr class="labletag">
        <th scope="col">عنوان</th>
        <th scope="col">لینک</th>
        <th scope="col">حذف</th>
      </tr>
    </thead>
    <tbody>
    <?php
            $slider="SELECT * FROM `slider` ORDER BY `id` DESC";
            $sliderquery=mysqli_query($link,$slider);
            while($sliderfetch=mysqli_fetch_assoc($sliderquery))
            {
                ?>
      <tr>
            <td scope="col"><a href=""><?php echo $sliderfetch["title"] ?></a></td>
            <td scope="col"><a href=""><?php echo $sliderfetch["src"] ?></a></td>
            <td scope="col"><a href=<?php echo "../check.php?sliderid=$sliderfetch[id]" ?>>حذف</a></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
    </div>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    </body>

</html>