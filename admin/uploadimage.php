<?php include('./menu.php') ?>
<div class="uploadbox text-center">
    <form method="post" enctype="multipart/form-data">
        <label for="file" class="uploagimg">آدرس عکس</label>
        <input type="file" name="file" id="file" class="fileupload">
        <input type="submit" id="uploadbtn" name="uploadbtn" value="آپلود" class="subupload">
    </form>

</div>

<div class="uploadbigbox">
    <?php

    if (isset($_POST["uploadbtn"])) {
        echo "<div class=uploadBigbox>";
        if ($_FILES["file"]['size'] < 10) {
            echo "<center>فایلی انتخاب نشده است</center>";
        } else {
            $filename = "../thump/" . $_FILES["file"]["name"];
            $filesize = $_FILES["file"]["size"];
            $filetype = $_FILES["file"]["type"];
            $filetmp = $_FILES["file"]["tmp_name"];

            if (is_uploaded_file($filetmp)) {
                if (move_uploaded_file($filetmp, $filename)) {
                    echo "<p>فایل با موفقیت آپلود شد</p>";
                    echo "<p>نام فایل : " . $filename . "</p>";
                    echo "<p>سایز فایل : " . $filesize . "</p>";
                    echo "<p>نوع فایل : " . $filetype . "</p>";
                    echo '<a href="' . $filename . '">ادرس فایل </a>';
                }
            } else {
                echo "<center>مشکل در آپلود</center>";
            }
        }
    }

    ?>
</div>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>