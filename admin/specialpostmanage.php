<?php include('menu.php');?>
<?php include('../config.php');?>
<?php
if(!isset($_COOKIE["admin"])){
header("location:../index.php");
exit;
}
?>

    <div class="container post-manage my-5 ">
        <label for="" class="labletag m-3 text-center" style="float: right;">تنظیمات پست های ویژه</label>
        <br>
        <?php
        if(isset($_GET["specialempty"])){
            echo"<center><font color=red>تمام فیلد ها باید پر باشند.</font></center>";
        }
        if(isset($_GET["file-error"])){
            echo"<center><font color=red>فرمت فایل اشتباه است.</font></center>";
        }
        if(isset($_GET["specialerror"])){
            echo"<center><font color=red> مشکل در ارسال اطلاعات</font></center>";
        }
        if(isset($_GET["specialok"])){
            echo"<center><font color=green>ارسال اطلاعات با موفقیت انجام شد.</font></center>";
        }
        ?>
        <div class="my-4 mx-auto p-4">
            <form method="post" action="../check.php" enctype="multipart/form-data" class="mb-3">
                <label for="exampleFormControlInput1" class="form-label fw-bold">عنوان</label>
                <input type="text" class="form-control " id="exampleFormControlInput1" name="specialposttitle">
                <label for="specialpostimage" class="form-label fw-bold">عکس</label>
                <input class="form-control" type="file" id="specialpostimage" name="specialpostimage">
                <!-- <input class="form-control" type="file" id="formFileMultiple" name="specialpostimage" multiple> -->
                <label for="exampleFormControlTextarea1" class="form-label fw-bold">محتوا</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="specialpostcontent"></textarea>
                <input type="submit" value="ارسال" class="btn btn-warning m-3" id="btnspecialpost" name="btnspecialpost">
                <a href="./uploadimage.php" type="button" class="btn btn-warning btn-rounded float-right py-2 px-4 "
                style="border-radius: 20px;">آپلود تصویر </a>
            </form>
        </div>
    </div>

    <body>
        <script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    </body>

</html>