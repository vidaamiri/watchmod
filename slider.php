<div class="container my-3">
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <?php
            $slider = "SELECT * FROM `slider` ORDER BY `id` DESC";
            $sliderresult = mysqli_query($link, $slider);
            $counter = 0;
            while ($sliderfetch = mysqli_fetch_assoc($sliderresult)) {
                if ($counter < 2){
                    ?>
                        <div class="carousel-item <?php if ($counter === 0){echo "active";} ?>">
                            <img src="uploads/images/slider/<?php echo $sliderfetch['src']; ?>" class="d-block w-100 d-none d-md-block" style="height: 500px;" alt="...">
                            <img src="uploads/images/slider/<?php echo $sliderfetch['src']; ?>" class="d-block w-100 d-none d-sm-block d-md-none" style="height: 350px;" alt="...">
                            <img src="uploads/images/slider/<?php echo $sliderfetch['src']; ?>" class="d-block w-100 d-black d-sm-none " style="height: 250px;" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5><?php echo $sliderfetch["title"]; ?></h5>
                            </div>
                        </div>
                    <?php
                    $counter++;
                }else{
                    break;
                }
            }
            ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>